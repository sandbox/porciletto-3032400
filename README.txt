When enabled, this module will skip any image effect for animated GIFs for all styles. This will prevent animated GIFs to be turned into static images, but of course won't apply any style. If possible, you should use ImageMagick with this patch: https://www.drupal.org/project/imagemagick/issues/1802534.

If you need something like this for D8, try https://www.drupal.org/project/animated_gif
